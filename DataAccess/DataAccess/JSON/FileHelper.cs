﻿using Common;
using Newtonsoft.Json;

namespace DataAccess.JSON
{
    public class FileHelper
    {
        private string _fileName = "";
        public FileHelper()
        {
            _fileName = "to_dos.json";
        }
        public FileHelper(string fileName)
        {
            _fileName = fileName;
        }
        public IEnumerable<ToDo>? GetListFromFile()
        {
            IEnumerable<ToDo>? toDos = null;
            using (StreamReader r = new StreamReader(_fileName))
            {
                string json;
                try
                {
                   json = r.ReadToEnd();
                }
                catch(FileNotFoundException ex)
                {
                    json = "";
                }
                toDos = JsonConvert.DeserializeObject<List<ToDo>>(json);
            }
            return toDos;
        }

        public void WriteToFile(IEnumerable<ToDo> toDos)
        {
            string json = JsonConvert.SerializeObject(toDos);
            File.WriteAllText(_fileName, json);
        }
    }
}
