﻿using Common;
using DataAccess.JSON;

namespace DataAccess
{
    public class ToDoRepo : IToDo
    {
        private readonly FileHelper _fileHelper;
        private List<ToDo>? ToDoList { get; set; }
        public ToDoRepo()
        {
            _fileHelper = new FileHelper();
            ToDoList = (List<ToDo>?)_fileHelper?.GetListFromFile();
        }
        public void DeleteAllToDos()
        {
            ToDoList?.Clear();
            Save();
        }

        public void DeleteToDo(ToDo toDo)
        {
            ToDoList?.Remove(toDo);
            Save();
        }

        public IEnumerable<ToDo>? GetAllToDos()
        {
            return ToDoList;
        }

        public ToDo? GetToDo(int id)
        {
            return ToDoList?.FirstOrDefault(x => x.Id == id);
        }

        public void AddToDo(ToDo toDo)
        {
            ToDoList?.Add(toDo);
            Save();
        }

        public void Save()
        {
            _fileHelper.WriteToFile(ToDoList);
        }
    }
}