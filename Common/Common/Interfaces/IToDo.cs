﻿namespace Common
{
    public interface IToDo
    {
        public IEnumerable<ToDo>? GetAllToDos();
        public void AddToDo(ToDo toDo);
        public ToDo? GetToDo(int id);
        public void DeleteAllToDos();
        public void DeleteToDo(ToDo toDo);
    }
}
