using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;
using DataAccess.JSON;
using System.IO;
using DataAccess;

namespace ToDoTest
{
    [TestClass]
    public class DataAccessTest
    {
        [TestMethod]
        public void GetListFromFile_FileNotFoundException()
        {
            List<ToDo> toDos;
            FileHelper fileHelper = new FileHelper("Alejandra.json");
            FileNotFoundException exception = null;
            try
            {
                toDos = (List<ToDo>)fileHelper.GetListFromFile();
                Assert.Fail("An exception should have been thrown");
            }
            catch (FileNotFoundException ex)
            {
                exception = ex;
            }
            catch(Exception ex)
            {
                Assert.Fail($"Unexpected exception of type {ex.GetType()} caught: {ex.Message}");
            }

            Assert.IsNotNull(exception);
        }

        [TestMethod]
        public void GetListFromFile_True()
        {
            List<ToDo>? toDos = null;
            FileHelper fileHelper = new FileHelper("test.json");
            FileNotFoundException exception = null;

            toDos = (List<ToDo>)fileHelper.GetListFromFile();

            Assert.AreEqual(4, toDos?.Count);
        }

        [TestMethod]
        public void ToDoRepoCreation_Success()
        {
            ToDoRepo _repo = new();
            List<ToDo> toDos = (List<ToDo>)_repo.GetAllToDos();

            Assert.AreEqual(4, toDos.Count);
        }

        [TestMethod]
        public void ToDoRepo_DeleteAllToDos()
        {
            ToDoRepo _repo = new();

            _repo.DeleteAllToDos();
            List<ToDo> toDos = (List<ToDo>)_repo.GetAllToDos();


            Assert.AreEqual(0, toDos.Count);
        }

        [TestMethod]
        public void ToDoRepo_DeleteToDo()
        {
            ToDoRepo _repo = new();
            List<ToDo> toDos = (List<ToDo>)_repo.GetAllToDos();
            _repo.DeleteToDo(toDos[0]);
            toDos = (List<ToDo>)_repo.GetAllToDos();
            Assert.AreEqual(3, toDos.Count);
            Assert.AreEqual(2, toDos[0].Id);
        }

        [TestMethod]
        public void ToDoRepo_GetToDo()
        {
            ToDoRepo _repo = new();

            ToDo expectedTodo = _repo.GetToDo(4);

            Assert.AreEqual(4, expectedTodo.Id);
        }

        [TestMethod]
        public void ToDoRepo_AddToDo()
        {
            ToDoRepo _repo = new();

            ToDo newToDo = new ToDo() { Id = 5, Title = "Test", Description="This is a test ToDo", IsFinished=true};

            _repo.AddToDo(newToDo);

            List<ToDo> toDos = (List<ToDo>)_repo.GetAllToDos();

            Assert.AreEqual(5, toDos.Count);
        }
    }
}